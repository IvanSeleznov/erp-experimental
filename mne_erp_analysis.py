import os.path as op
import numpy as np
import json, os, glob
import matplotlib.pyplot as plt
import mne
import pyedflib, pickle
import json

def get_data_from_edf_file(filename):
    f = pyedflib.EdfReader(filename)
    NumberOfSignals = f.signals_in_file
    BirthDate = f.birthdate
    Gender = f.gender
    Name = f.patientname
    SignalLabels = f.getSignalLabels()
    NumberOfSamp = f.getNSamples()[0]
    SignalList = np.zeros((NumberOfSignals, f.getNSamples()[0]))
    Fs = round(len(SignalList[3, :]) / f.file_duration)

    for i in np.arange(NumberOfSignals):
        SignalList[i, :] = f.readSignal(i)

    return SignalList, Fs, SignalLabels, NumberOfSignals, BirthDate, Gender, Name


def convert_100ns_units_to_secs(val):
    return val * 10e-8


def parse_stim_file(filepath=None):
    unique_stimuli = {}
    stim_sequence = []

    with open(filepath) as f:
        for line in f:
            splitted_line = line.split()
            if len(splitted_line) == 3:
                start_t, stop_t, stim_name = splitted_line

                stim_sequence.append(
                    (convert_100ns_units_to_secs(int(start_t)), convert_100ns_units_to_secs(int(stop_t)), stim_name))

                if stim_name not in unique_stimuli:
                    unique_stimuli.update({stim_name: []})

                unique_stimuli[stim_name].append(
                    (convert_100ns_units_to_secs(int(start_t)),
                     convert_100ns_units_to_secs(int(stop_t)))
                )

    return unique_stimuli, stim_sequence


def get_time_shift(first_stim, signal):
    stim = np.where(np.diff(signal) > 0.5)
    time_shift = stim[0][0] - first_stim

    return time_shift

def dict_to_json(dict_to_save=None, filename="default.json"):
    # with open(filename, 'w') as fp:
        # json.dump(dict_to_save, fp)
    with open(filename, "wb") as f:
        f.write(json.dumps(dict_to_save).encode("utf-8"))

def visualize_erp(erp_dict=None, plt_pos=None):
    """

    :param erp_dict: dictionary of erp in the following form:
                        erp_dict : {
                            "stim#1" : {
                                "electrode#1" : {np.array},
                                "..." : {...}
                            },
                            "stim#2" : {
                                .....
                            }
                        }
    :return:
    """

    for i, stim_name in enumerate(erp_dict.keys()):

        if i < 3:
            plot_counter = 1
            plt.figure()
            plt.suptitle(stimuli_name)

            for j, electrode in enumerate(erp_dict[stim_name].keys()):

                if plot_counter < len(erp_dict[stim_name].keys()):
                    plt.subplot(plt_pos[0], plt_pos[1], plot_counter)
                    plt.title(electrode)

                    #get the mean erp
                    # print(erp_dict[stim_name][electrode].shape)
                    x_shape = erp_dict[stim_name][electrode].shape
                    erp_final = np.sum(erp_dict[stim_name][electrode], axis=0)/x_shape[0]
                    plt.plot(erp_final)
                    plt.grid(True)

                    plot_counter += 1


if __name__ == '__main__':

    config = json.loads(open("config.json").read())

    current_CWD = os.getcwd()

    data_dir = current_CWD + config.get("data_folder")
    output_dir = current_CWD + config.get("output_folder")
    debug = config.get("debug")
    eeg_file_to_parse = config.get("data_file_to_parse")
    stim_file_to_parse = config.get("stumuli_file_to_parse")
    filename = eeg_file_to_parse.rsplit(".", 1)[0]

    # get the files list and stim files list
    files = sorted(glob.glob(data_dir + "/*.edf"))
    stim_files = sorted(glob.glob(data_dir + "/*.htk"))

    # get data from edf file
    if eeg_file_to_parse in [i.rsplit('/', 1)[1] for i in files]:
        SignalList, Fs, SignalLabels, NumberOfSignals, BirthDate, Gender, Name = get_data_from_edf_file(data_dir+eeg_file_to_parse)
    else:
        SignalList, Fs, SignalLabels, NumberOfSignals, BirthDate, Gender, Name = get_data_from_edf_file(files[0])

    #get data from stimuli file
    if stim_file_to_parse in [j.rsplit('/', 1)[1] for j in stim_files]:
        stim_dict, stim_seq = parse_stim_file(data_dir+stim_file_to_parse)
    else:
        stim_dict, stim_seq = parse_stim_file(stim_files[0])

    print(stim_dict)


    dict_to_json(dict_to_save=stim_dict, filename=data_dir+filename+".stimulations.json")

    # get timeshift between the beginning of the recording and the beginning of the stimuli
    time_shift = get_time_shift(stim_seq[0][0] * 500, SignalList[-1, :])

    if debug:
    # # plot some the stim raw data and the timestamps
        plt.figure()
        plt.plot(SignalList[-1, :])
        for i, stim in enumerate(stim_seq):
            if i <= 10:
                plt.axvline(x=stim[0] * 500 + time_shift, color="r")
                plt.axvline(x=stim[1] * 500 + time_shift, color="g")
        plt.show()


    #create a stimuli vector to show the beginneing and the end of each stimuli
    stimuli_vector = np.zeros((1, SignalList.shape[1]), dtype=int)
    stim_to_id_dict = {}
    stim_id_counter = 1
    current_stimuli_id = 0
    for i, stim in enumerate(stim_seq):

        if not stim[2] in stim_to_id_dict.keys():
            stim_to_id_dict.update({stim[2] : stim_id_counter})
            current_stimuli_id = stim_id_counter
            stim_id_counter += 1
        else:
            current_stimuli_id = stim_to_id_dict[stim[2]]

        stim_start = int(stim[0] * 500 + time_shift)
        stim_stop = int(stim[1] * 500 + time_shift)
        stimuli_vector[0, stim_start] = current_stimuli_id
        stimuli_vector[0, stim_stop] = current_stimuli_id

    #add stimuli vector to signal matrix
    eeg_stim_array = np.concatenate((SignalList, stimuli_vector), axis=0)

    # Save data matrix to csv file
    np.savetxt(fname=data_dir+filename+'.eeg_stim.csv',
               X=eeg_stim_array.T,
               fmt="%.4f",
               delimiter=',',
               header=','.join(SignalLabels)+",EEG Stimuli")

    #save file with stimuli id
    dict_to_json(dict_to_save=stim_to_id_dict, filename=data_dir+filename+".stim_id.json")

    # get the len of the longest stimuli
    longest_stim_duration = 0
    for i, st in enumerate(stim_seq):
        stim_duration = st[1] - st[0]
        if stim_duration > longest_stim_duration:
            longest_stim_duration = stim_duration

    # change from time to countdown numbers
    longest_stim_duration = int(longest_stim_duration * Fs)
    before_cnt_lag = int(config.get("before_stim_time_lag") * Fs)
    after_cnt_lag = int(config.get("after_stim_time_lag") * Fs)

    total_erp_len = int(longest_stim_duration + before_cnt_lag + after_cnt_lag)

    # plt.show()

    #get all the ERP to dictionary
    erp_stim = {}
    for i, stimuli_name in enumerate(stim_dict.keys()):
        if stimuli_name not in erp_stim.keys():
            erp_stim.update({stimuli_name: {}})

        #got through all the electrode labels
        for s, sigLabel in enumerate(SignalLabels):
            erp_stim[stimuli_name].update({sigLabel: np.zeros((len(stim_dict[stimuli_name]), total_erp_len), float)})

            # go through all the stimuli and copy them to the dictionary
            for j, st in enumerate(stim_dict[stimuli_name]):

                start_t, stop_t = stim_dict[stimuli_name][j]

                start_c = int(start_t * Fs) - before_cnt_lag
                stop_c = int(start_t * Fs) + longest_stim_duration + after_cnt_lag

                erp_stim[stimuli_name][sigLabel][j] = SignalList[s, start_c:stop_c].reshape((1, 1, -1)).copy()

    #save erp-dict to json file
    # dict_to_json(dict_to_save=erp_stim, filename=data_dir+"erp.json")

    #plot the results
    # visualize_erp(erp_dict=erp_stim, plt_pos=(4, 5))
    # plt.show()

    print("Done")
